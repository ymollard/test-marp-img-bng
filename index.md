---
marp: true
---
<!-- 
class: invert
paginate: true
title: test image background
-->

<style>
    .hljs-string {
    color: #cd9067;
}

section {
  background-image: url('img/red-green-blue-black-background.svg');
  background-size: cover;
}

</style>

This SVG image of the Python logo `![](img/logo.svg)` has a transparent background that breaks the red-green-blue-black slide background:

![](img/logo.svg)

---

When used as a background, the problem does not appear:
`![bg right 90%](img/logo.svg)`

![bg right 90%](img/logo.svg)

---

Demonstrated with class `invert` but the default class behaves the same.

Demonstrated with SVG files but PNG and WEBP behave the same.